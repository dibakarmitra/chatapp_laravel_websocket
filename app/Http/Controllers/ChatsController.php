<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Events\PrivateMessageSent;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatsController extends Controller
{
    // public $message;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chats');
    }
    
    public function private() {
        
        return view('private');
    }
    
    public function users(){
        return User::all();
    }

    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    public function fetchPrivateMessages(User $user)
    {
        return Message::with('user')
            ->where(['user_id' => Auth::id(), 'to_id' => $user->id])
            ->orWhere(function ($query) use ($user) {
                $query->where(['user_id' => $user->id, 'to_id' => Auth::id()]);
            })
            ->get();
    }

    public function sendMessage(Request $request)
    {
        $message = Auth::user()->messages()->create([
            'message' => $request->message
        ]);

        broadcast(new MessageSent($message->load('user')))->toOthers();

        return ['status' => 'success'];
    }

    public function sendPrivateMessage(Request $request, User $user)
    {
        $message = Auth::user()->messages()->create([
            'user_id' => Auth::id(),
            'message' => $request->message,
            'to_id' => $user->id,
        ]);

        broadcast(new PrivateMessageSent($message->load('user')))->toOthers();

        return ['status' => 'success'];
    }
}
