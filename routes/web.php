<?php

use App\Events\WebsocketDemoEvent;
use App\Http\Controllers\ChatsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    broadcast(new WebsocketDemoEvent('any data'));
    return view('welcome');
});

Route::get('/chats', [\App\Http\Controllers\ChatsController::class, 'index'])->name('chats');
Route::get('/private', [\App\Http\Controllers\ChatsController::class, 'private'])->name('private');
Route::get('/users', [\App\Http\Controllers\ChatsController::class, 'users'])->name('users');


Route::get('/messages', [\App\Http\Controllers\ChatsController::class, 'fetchMessages'])->name('fetchMessages');
Route::post('/messages', [\App\Http\Controllers\ChatsController::class, 'sendMessage'])->name('sendMessages');


Route::get('/private/messages/{user}', [\App\Http\Controllers\ChatsController::class, 'fetchPrivateMessages'])->name('fetchPrivateMessages');
Route::post('/private/messages/{user}', [\App\Http\Controllers\ChatsController::class, 'sendPrivateMessage'])->name('sendPrivateMessages');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
